﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class gantiGambar : MonoBehaviour {
	public Image testImage;

	public void setGambar(string namaGambar){
		Sprite newSprite =  Resources.Load <Sprite>(namaGambar);
		if (newSprite){
			testImage.sprite = newSprite;
		} else {
			Debug.LogError("Sprite not found", this);
		}
	}

}
