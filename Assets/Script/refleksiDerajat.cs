﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class refleksiDerajat  : MonoBehaviour {
	public Transform rotasiDerajat;
	bool btar = false;
	public float speedFactor = 0.02f;
	
	
	// Use this for initialization
	
	
	// Update is called once per frame
	void Update () {
		if (btar) {
			transform.rotation = Quaternion.Slerp (transform.rotation, rotasiDerajat.rotation, speedFactor);
		}
		
		
	}
	
	public void simulasi(){
		btar = true;
		
	}

	public void reset(){
		btar = false;
		
	}
}

