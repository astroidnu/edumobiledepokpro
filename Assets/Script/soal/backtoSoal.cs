﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class backtoSoal : MonoBehaviour {
	public Button btnBack;
	public Canvas review;
	public Canvas canvasSoal1;

	// Use this for initialization
	void Start () {
		btnBack = GameObject.Find ("back-button").GetComponent<Button> ();
		review = GameObject.Find ("Review").GetComponent<Canvas> ();
		canvasSoal1 = GameObject.Find ("CanvasSoal1").GetComponent<Canvas> ();
		btnBack.onClick.AddListener (delegate() {
			review.enabled = false;
			canvasSoal1.enabled = true;
			
		});
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
