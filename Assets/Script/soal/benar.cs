﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class benar : MonoBehaviour {
	public Button benar2;
	public SpriteRenderer salah_1;
	public SpriteRenderer benar_1;
	public Canvas canvasSoal1;
	public Canvas canvasHasil;
	public SpriteRenderer header_1;
	public SpriteRenderer soal_1;
	public SpriteRenderer background_hasil;
	public bool aktif;


	// Use this for initialization
	void Start () {
		benar2 = GameObject.Find("1_a").GetComponent<Button> ();
		canvasSoal1 = GameObject.Find ("CanvasSoal1").GetComponent<Canvas> ();
		header_1 = GameObject.Find ("1_header").GetComponent<SpriteRenderer> ();
		soal_1 = GameObject.Find ("1_soal").GetComponent<SpriteRenderer> ();
		canvasHasil = GameObject.Find ("Review").GetComponent<Canvas> ();
		background_hasil = GameObject.Find ("background_hasil").GetComponent<SpriteRenderer> ();
		benar_1 = GameObject.Find ("1_benar").GetComponent<SpriteRenderer> ();
		salah_1 = GameObject.Find ("1_salah").GetComponent<SpriteRenderer> ();
		canvasHasil.enabled = false;

		background_hasil.enabled = false;
		benar2.onClick.AddListener (delegate() {
			aktif = true;
			canvasSoal1.enabled = false;
			header_1.enabled = false;
			soal_1.enabled = false;
			canvasHasil.enabled = true;
			background_hasil.enabled = true;
			benar_1.enabled = true;
		
		});
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
