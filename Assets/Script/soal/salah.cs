﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class salah : MonoBehaviour {
	public Button salah2;
	public Button salah3;
	public Button salah4;
	public SpriteRenderer benar_1;
	public SpriteRenderer salah_1;
	public Canvas canvasSoal1;
	public Canvas canvasHasil;
	public SpriteRenderer header_1;
	public SpriteRenderer soal_1;
	public SpriteRenderer header_2;
	public SpriteRenderer background_hasil;

	// Use this for initialization
	void Start () {
		salah2 = GameObject.Find("1_b").GetComponent<Button> ();
		salah3 = GameObject.Find("1_c").GetComponent<Button> ();
		salah4 = GameObject.Find("1_d").GetComponent<Button> ();
		canvasSoal1 = GameObject.Find ("CanvasSoal1").GetComponent<Canvas> ();
		header_1 = GameObject.Find ("1_header").GetComponent<SpriteRenderer> ();
		soal_1 = GameObject.Find ("1_soal").GetComponent<SpriteRenderer> ();
		canvasHasil = GameObject.Find ("Review").GetComponent<Canvas> ();
		background_hasil = GameObject.Find ("background_hasil").GetComponent<SpriteRenderer> ();
		salah_1 = GameObject.Find ("1_salah").GetComponent<SpriteRenderer> ();
		benar_1 = GameObject.Find ("1_benar").GetComponent<SpriteRenderer> ();
		canvasHasil.enabled = false;
		
		background_hasil.enabled = false;
		salah2.onClick.AddListener (delegate() {
			canvasSoal1.enabled = false;
			header_1.enabled = false;
			soal_1.enabled = false;
			canvasHasil.enabled = true;
			background_hasil.enabled = true;
			salah_1.enabled = true;
			
		});
		salah3.onClick.AddListener (delegate() {
			canvasSoal1.enabled = false;
			header_1.enabled = false;
			soal_1.enabled = false;
			canvasHasil.enabled = true;
			background_hasil.enabled = true;
			benar_1.enabled = false;
			salah_1.enabled = true;
			
		});
		salah4.onClick.AddListener (delegate() {
			canvasSoal1.enabled = false;
			header_1.enabled = false;
			soal_1.enabled = false;
			canvasHasil.enabled = true;
			background_hasil.enabled = true;
			benar_1.enabled = false;
			salah_1.enabled = true;
			
		});
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
