﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class backToMenuSimulasi : MonoBehaviour {
	public Button backmenu;
	// Use this for initialization
	void Start () {
		backmenu = GameObject.Find ("backMenu").GetComponent<Button>();
		backmenu.onClick.AddListener (delegate() {
			Application.LoadLevel ("menuSimulasi");
		});
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
