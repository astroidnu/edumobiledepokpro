﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class menuHandle2 : MonoBehaviour {
	public Button btnTranslasi;
	public Button btnRefleksi;
	public Button btnRotasi;
	public Button btnDilatasi;
	// Use this for initialization
	void Start () {
		btnTranslasi = GameObject.Find ("btnTranslasi").GetComponent<Button>();
		btnRefleksi = GameObject.Find ("btnRefleksi").GetComponent<Button>();
		btnRotasi = GameObject.Find ("btnRotasi").GetComponent<Button>();
		btnDilatasi = GameObject.Find ("btnDilatasi").GetComponent<Button>();
		
		btnTranslasi.onClick.AddListener (delegate() {
			Application.LoadLevel("RumusTranslasi");
		});
		btnRefleksi.onClick.AddListener (delegate() {
			Application.LoadLevel("RumusRefleksi");
		});
		btnRotasi.onClick.AddListener (delegate() {
			Application.LoadLevel("RumusRotasi");
		});
		btnDilatasi.onClick.AddListener (delegate() {
			Application.LoadLevel("RumusDilatasi");
		});
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
