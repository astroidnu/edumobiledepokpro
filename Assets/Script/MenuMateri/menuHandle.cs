﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class menuHandle : MonoBehaviour {
	public Button btnTranslasi;
	public Button btnRefleksi;
	public Button btnRotasi;
	public Button btnDilatasi;
	// Use this for initialization
	void Start () {
		btnTranslasi = GameObject.Find ("btnTranslasi").GetComponent<Button>();
		btnRefleksi = GameObject.Find ("btnRefleksi").GetComponent<Button>();
		btnRotasi = GameObject.Find ("btnRotasi").GetComponent<Button>();
		btnDilatasi = GameObject.Find ("btnDilatasi").GetComponent<Button>();

		btnTranslasi.onClick.AddListener (delegate() {
			Application.LoadLevel("trans1");
		});
		btnRefleksi.onClick.AddListener (delegate() {
			Application.LoadLevel("menuSimulasi");
		});
		btnRotasi.onClick.AddListener (delegate() {
			Application.LoadLevel("menuSimulasi");
		});
		btnDilatasi.onClick.AddListener (delegate() {
			Application.LoadLevel("menuSimulasi");
		});
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
