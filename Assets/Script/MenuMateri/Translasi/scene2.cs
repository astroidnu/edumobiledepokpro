﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class scene2 : MonoBehaviour {
	public Button btnPrev;
	public Button btnNext;
	// Use this for initialization
	void Start () {
		btnPrev = GameObject.Find ("btnBack").GetComponent<Button> ();
		btnPrev.onClick.AddListener (delegate {
			Application.LoadLevel("trans1");
		});
		btnNext = GameObject.Find ("btnNext").GetComponent<Button> ();
		btnNext.onClick.AddListener (delegate {
			Application.LoadLevel("trans3");
		});
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
