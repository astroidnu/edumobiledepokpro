﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RefleksiScene : MonoBehaviour {
	public Text txta,txtb,txth,txtk,txtpilih,txtahapus,txtbhapus;
	public Text txta1,txta2,txta3,txta4,txta5,txta6,txta7,txtb1,txtb2,txtb3,txtb4,txtb5,txtb6,txtb7,txtx1,txtx2,txtx3,txtx4,txtx5,txtx6,txtx7,txtxy1,txtxy2,txtxy3,txtxy4,txtxy5,txtxy6,txtxy7,txtxh,txtxk;
	float a,b,h,k;
	string pilihaan;
	public Text txtsbxx,txtsbxy;
	///deklarasi nilai akhir;
	float sbxx = 0,sbxy = 0;
	string pilihan;
	bool simul = false;
	//float menambah kalo min
	float xtemp,ytemp,atemp,btemp;

	//terhadap sumbu y
	float sbyx,sbyy;

	//untuk transform
	public GameObject cubee,cubetranform,cubeeBayangan, cubetranslasi1,gambar,gambar2;
	// Use this for initialization
	void Start () {
	
	}

	public void tombolSimul(){
		simul = true;
	}

	public void tombolReset(){
		simul = false;
		txtahapus.text = "0";
		txtbhapus.text = "0";
		txtsbxx.text = "0";
		txtsbxy.text = "0";
		txta.text = "0";
		txtb.text = "0";
		txth.text = "0";
		txtk.text = "0";
		txta1.text = "0";
		txta2.text = "0";
		txta3.text = "0";
		txta4.text = "0";
		txta5.text = "0";
		txta6.text = "0";
		txta7.text = "0";
		txtb1.text = "0";
		txtb2.text = "0";
		txtb3.text = "0";
		txtb4.text = "0";
		txtb5.text = "0";
		txtb6.text = "0";
		txtb7.text = "0";
		txtx1.text = "0";
		txtx2.text = "0";
		txtx3.text = "0";
		txtx4.text = "0";
		txtx5.text = "0";
		txtx6.text = "0";
		txtx7.text = "0";
		txtxy1.text = "0";
		txtxy2.text = "0";
		txtxy3.text = "0";
		txtxy4.text = "0";
		txtxy5.text = "0";
		txtxy6.text = "0";
		txtxy7.text = "0";
		txtxk.text = "0";
		txtxh.text = "0";

		txtpilih.text = "";
		gambar.transform.eulerAngles = new Vector3(0,0,0);
		gambar2.transform.eulerAngles = new Vector3(0,0,0);
	}
	
	// Update is called once per frame
	void Update () {
		txta1.text = txta.text;
		txta2.text = txta.text;
		txta3.text = txta.text;
		txta4.text = txta.text;
		txta5.text = txta.text;
		txta6.text = txta.text;
		txta7.text = txta.text;
		txtb1.text = txtb.text;
		txtb2.text = txtb.text;
		txtb3.text = txtb.text;
		txtb4.text = txtb.text;
		txtb5.text = txtb.text;
		txtb6.text = txtb.text;
		txtb7.text = txtb.text;
		txtxh.text = txth.text;
		txtxk.text = txtk.text;
		a = float.Parse (txta.text);
		b = float.Parse (txtb.text);
		h = float.Parse (txth.text);
		k = float.Parse (txtk.text);
		pilihan = txtpilih.text;
		if (pilihan == "Sb-X") {
			//untuk terhadap sb x
			sbxx = (1 * a) + (0 * b);
			sbxy = (0 * a) + (-1 * b);
			txtx1.text = sbxx.ToString ();
			txtxy1.text = sbxy.ToString ();
		} else if (pilihan == "Sb-Y") {
			sbxx = (-1 * a) + (0 * b);
			sbxy = (0 * a) + (1 * b);
			txtx2.text = sbxx.ToString ();
			txtxy2.text = sbxy.ToString (); 
		} else if (pilihan == "Y = X") {
			sbxx = (0 * a) + (1 * b);
			sbxy = (1 * a) + (0 * b);
			txtx5.text = sbxx.ToString ();
			txtxy5.text = sbxy.ToString ();
		} else if (pilihan == "Y = -X") {
			sbxx = (0 * a) + (-1 * b);
			sbxy = (-1 * a) + (0 * b);
			txtx6.text = sbxx.ToString ();
			txtxy6.text = sbxy.ToString ();
		} else if (pilihan == "X = h atau -h") {
			sbxx = (-1	 * a) + (0 * b) + (2 * h);
			sbxy = (0 * a) + (1 * b);
			txtx3.text = sbxx.ToString ();
			txtxy3.text = sbxy.ToString ();
		} else if (pilihan == "Y = k atau -k") {
			sbxx = (1 * a) + (0 * b);
			sbxy = (0 * a) + (-1 * b) + (2 * k);
			txtx4.text = sbxx.ToString ();
			txtxy4.text = sbxy.ToString ();
		} else if (pilihan == "O(0,0)") {
			sbxx = (-1 * a) + (0 * b);
			sbxy = (0 * a) + (-1 * b);
			txtx7.text = sbxx.ToString ();
			txtx7.text = sbxy.ToString ();
		} else {
			sbxx = 0;
			sbxy = 0;
		}

		//menambah 0.5 kalo minus

		if (sbxx < 0) {
			xtemp = 0.5f;
		} else {
			xtemp = 0;
		}
		if (sbxy < 0) {
			ytemp = 0.5f;
		} else {
			ytemp = 0;
		}

		if (a < 0) {
			atemp = 0.5f;
		} else {
			atemp = 0;
		}
		if (b < 0) {
			btemp = 0.5f;
		} else {
			btemp = 0;
		}

		if (simul) {
			cubee.transform.position = new Vector3 (sbxx, sbxy, -0.4f);
			if (pilihan == "Sb-X") {
				cubee.transform.position = new Vector3 (sbxx * -1f + xtemp , sbxy-4f -ytemp, -0.4f);
				gambar.transform.eulerAngles = new Vector3(180f,0,0);
			} else if (pilihan == "Sb-Y") { 
				cubee.transform.position = new Vector3 (sbxx * -1f + xtemp + 9f , sbxy -ytemp, -0.4f);
				gambar.transform.eulerAngles = new Vector3(0,180f,0);
			} else if (pilihan == "Y = X") { 
				//inibelum
				cubee.transform.position = new Vector3 (sbxx * -1f + xtemp  + 2f  , sbxy +2f -ytemp, -0.4f);
				gambar.transform.eulerAngles = new Vector3(0,180f,270f);
			} else if (pilihan == "Y = -X") { 
				cubee.transform.position = new Vector3 (sbxx * -1f + xtemp  + 6.6f  , sbxy -5.6f -ytemp, -0.4f);
				gambar.transform.eulerAngles = new Vector3(0,180f,90f);
			} else if (pilihan == "X = h atau -h") {
				cubee.transform.position = new Vector3 (sbxx * -1f + xtemp + 9f , sbxy-0f -ytemp, -0.4f);
				gambar.transform.eulerAngles = new Vector3(0,180f,0);
			} else if (pilihan == "Y = k atau -k") {
				cubee.transform.position = new Vector3 (sbxx * -1f + xtemp, sbxy -ytemp -4f, -0.4f);
				gambar.transform.eulerAngles = new Vector3(180f,0,0);
			} else if (pilihan == "O(0,0)") {
				cubee.transform.position = new Vector3 (sbxx * -1f + xtemp  +9f  , sbxy  -ytemp -4f, -0.4f);
				gambar.transform.eulerAngles = new Vector3(0,0,180f);
			}
		} else {
			cubee.transform.position = new Vector3 (a * -1f, b - btemp, -0.4f);
			cubetranform.transform.position = new Vector3 (a * -1f, b - btemp, -0.4f);
			gambar.transform.eulerAngles = new Vector3(0,0,0);
			gambar2.transform.eulerAngles = new Vector3(0,0,0);
		}


		if (pilihan == "") {
			cubetranslasi1.transform.position = new Vector3 (0,0, -0.4f);
		}  else {
			cubetranslasi1.transform.position = new Vector3 (sbxx * -1f + xtemp, sbxy - ytemp, -0.4f);
		}
		cubeeBayangan.transform.position = new Vector3 (a * -1f, b, -0.4f);

		//y=x

		//y=-x

		//x = h atau -h

		//y = k atau -k

		//terhadap sumbu o
	}
}
