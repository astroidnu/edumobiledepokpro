﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DilatasiPerhitungan : MonoBehaviour {
	public Text txta,txtb,txtm,txtn,txtk,txtx1,txty1;
	public Text txttempa,txttempb,txttempm,txttempn,txttempk,txttempm1,txttempn1,txttempk1,txttempx1,txttempy1;
	float a,b,m,n,k,m1,n1,k1,x1,y1;
	float x1f,y1f;
	bool simulasi = false;
	public GameObject cubee,cubeeOu, cubeeAsal, cubetranslasi1,cubetranslasi2,gambar;
	float speedFactor1 = 0.900f;
	// Use this for initialization
	void Start () {
	
	}

	public void buttonSimulasi(){
		simulasi = true;
	}
	public void buttonReset(){
		simulasi = false;
		txta.text = "0";
		txtb.text = "0";
		txtm.text = "0";
		txtn.text = "0";
		txtk.text = "0";
		txtm.text = "0";
		txtn.text = "0";
		txtk.text = "0";
		txttempx1.text = "0";
		txttempy1.text = "0";
	}
	
	// Update is called once per frame
	void Update () {
		//ini mau text
		txttempa.text = txta.text;
		txttempb.text = txtb.text;
		txttempm.text = txtm.text;
		txttempn.text = txtn.text;
		txttempk.text = txtk.text;
		txttempm1.text = txtm.text;
		txttempn1.text = txtn.text;
		txttempk1.text = txtk.text;

		//ini inisialisasi nilai
		a = float.Parse (txta.text);
		b = float.Parse (txtb.text);
		m = float.Parse (txtm.text);
		n = float.Parse (txtn.text);
		k = float.Parse (txtk.text);
		m1 = float.Parse (txtm.text);
		n1 = float.Parse (txtn.text);
		k1 = float.Parse (txtk.text);
		x1 = (k*(a-m))+(0*(b-n))+m;
		y1 = (0*(a-m))+(k*(b-n))+n; 
		txttempx1.text = x1.ToString();
		txttempy1.text = y1.ToString();
		//membuat titik tepat
		if (x1 < 0 ){
			x1f = x1 - 0.5f;
		}
		else{
			x1f = x1;
		}
		if (y1 < 0 ){
			y1f = y1 -0.5f;
		}
		else{
			y1f = y1;
		}

		if (simulasi) {
			//cubetranslasi1.transform.position = new Vector3 (x1 * -1f, y1, -0.4f);
			if (k != 0f){
				gambar.transform.localScale = new Vector3 (k, k);
			}


			if(k== 2f){
				cubeeOu.transform.position = new Vector3 ((x1f * -1f - (k*2.5f)), y1f+ k, -0.4f);
			}
			else if(k == 1.5f){
				cubeeOu.transform.position = new Vector3 ((x1f * -1f - (k*1.75f)), y1f+ 1f, -0.4f);
			}
			else if(k == 0.5f){
				cubeeOu.transform.position = new Vector3 (x1f * -1f +2.25f , y1f - 1.25f, -0.4f);
			}
			else if(k == 0f){
				cubeeOu.transform.position = new Vector3 (x1f * -1f , y1f, -0.4f);
			}
			else if(k == -0.5f){
				cubeeOu.transform.position = new Vector3 (x1f * -1f +6.5f , y1f -3f, -0.4f);
			}
			else if(k == -1.5f){
				cubeeOu.transform.position = new Vector3 (x1f * -1f +11.75f , y1f -5f, -0.4f);
			}
			else if(k == -2f){
				cubeeOu.transform.position = new Vector3 (x1f * -1f +13.5f , y1f -6f, -0.4f);
			}


		} else {
			cubee.transform.position = new Vector3 (a*-1f,b, -0.4f);
			cubeeOu.transform.position = new Vector3 (a*-1f,b, -0.4f);
			cubeeAsal.transform.position = new Vector3 (a*-1f,b, -0.4f);
			gambar.transform.localScale = new Vector3 (1f, 1f);
		}

		cubetranslasi1.transform.position = new Vector3 (x1f * -1f, y1f, -0.4f);
	}
}
