﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RotasiTitikGerak : MonoBehaviour {
	// nilai awal
	float xx = 0f, yy = 0f ,zz = -0.3f;
	//nilai pertama kotak
	float a = 0f, b = 0f;
	float m = 0f, n = 0f;
	float alpha = 0f, beta = 0f;
	//cepat berjalan
	public float speedFactor1 = 0.900f;
	//hasilperhitungan
	float hasilxx1 = 0f ,hasilyy1 = 0f;
	//teks untuk rumus
	//public Text txta,txt2b,txt3m,txt4n,txt5alpa, txt6beta;
	// Use this for initialization
	//settext rumus
	public Text ta,tb,tbeta,talpa,tm,tn,h1,h2;
	public GameObject cubeeRotasi, cubeRotasi1,cuberotasiimage;//cuberotasiimage untuk perputaran nanti
	bool btnklik =false;

	
	// Update is called once per frame
	void Update () {
		if (btnklik) {

		} else {
			cubeeRotasi.transform.position = new Vector3 (float.Parse(ta.text)*-1f,float.Parse(tb.text),-0.4f);
		}
		cubeRotasi1.transform.position = new Vector3 (hasilxx1*-1f,hasilyy1, -0.4f);
		cuberotasiimage.transform.eulerAngles = new Vector3(0,0,float.Parse(talpa.text));
	}

	public void klikbtn(){
		btnklik = true;
	}

	public void refreshHitungRotasi(){
		a = float.Parse (ta.text);
		b = float.Parse (tb.text);
		m = float.Parse (tm.text);
		n = float.Parse (tn.text);
		alpha = float.Parse (talpa.text);
		beta = float.Parse (tbeta.text);
		hasilxx1 = ((a-m)*Mathf.Cos(alpha + beta))-((b-n)*Mathf.Sin(alpha+beta));
		hasilyy1 = ((a-m)*Mathf.Sin(alpha + beta))+((b-n)*Mathf.Cos(alpha+beta));
		h1.text = hasilxx1.ToString ();
		h2.text = hasilyy1.ToString ();
	}
}
