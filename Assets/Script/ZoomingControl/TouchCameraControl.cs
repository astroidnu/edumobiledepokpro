﻿using UnityEngine;
using System.Collections;

public class TouchCameraControl : MonoBehaviour {
	public float moveSensitivityX = 1.0f;
	public float moveSensitivityY = 1.0f;
	public bool updateZoomSensitivity = true;
	public float orthoZoomSpeed = 0.5f;
	public float minZoom = 1.0f;
	public float maxZoom = 500.0f;
	public bool invertMoveX = false;
	public bool invertMoveY = false;

	private Transform _transform;
	private Camera _camera;

	// Use this for initialization
	void Start () {
		_transform = transform;
		_camera = Camera.main;

	}
	
	// Update is called once per frame
	void Update () {
		if (updateZoomSensitivity) {
			moveSensitivityX = _camera.orthographicSize;
			moveSensitivityY = _camera.orthographicSize;

		}
		//nilai sentuhan dimasukkan ke dalam array
		Touch[] touches = Input.touches;
		if (touches.Length > 0) {
			//ini kalau dengan 1 sentuhan
			if(touches.Length==1){
				if(touches[0].phase == TouchPhase.Moved){
					Vector2 delta = touches[0].deltaPosition;

					float positionX = delta.x* moveSensitivityX * Time.deltaTime;
					positionX = invertMoveX ? positionX : positionX * -1;

					float positionY = delta.y* moveSensitivityY * Time.deltaTime;
					positionY = invertMoveY ? positionY : positionY * -1;

					_camera.transform.position += new Vector3 (positionX,positionY,0);

				}

			}
			//ini kalau dengan 1 sentuhan
			if(touches.Length==2){

				//Vector2 cameraViewSize = new Vector2 (_camera.pixelWidth,_camera.pixelHeight);

				Touch touchOne = touches[0];
				Touch touchTwo = touches[1];

				Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
				Vector2 touchTwoPrevPos = touchTwo.position - touchTwo.deltaPosition;

				float prevTouchDeltaMag = (touchOnePrevPos - touchTwoPrevPos).magnitude;
				float touchDeltaMag = (touchOne.position - touchTwo.position).magnitude;

				float deltaMagDiff = prevTouchDeltaMag - touchDeltaMag;

				//_camera.transform.position += _camera.transform.TransformDirection((touchOnePrevPos + touchTwoPrevPos - cameraViewSize * _camera.orthographicSize / cameraViewSize.y));

				_camera.orthographicSize += deltaMagDiff * orthoZoomSpeed;
				_camera.orthographicSize = Mathf.Clamp (_camera.orthographicSize,minZoom, maxZoom);

				// _camera.transform.position -= _camera.transform.TransformDirection((touchOne.position + touchTwo.position - cameraViewSize) * _camera.orthographicSize/ cameraViewSize.y);
			}
		}
	}
}
