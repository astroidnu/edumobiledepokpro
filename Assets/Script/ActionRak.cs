﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ActionRak : MonoBehaviour {
	public Canvas canvasPause;
	public Canvas canvasMenu;
	public SpriteRenderer gambarPause;
	public SpriteRenderer bgGelap;
	public Button btn_kd;
	public Button btn_materi;
	public Button btn_video;
	public Button btn_simulasi;
	public Button btn_evaluasi;
	public Button btn_artikel;
	public Button btn_download;
	public Button btn_kumpulanrumus;
	public Button btn_kaleng;
	// Use this for initialization
	void Start () {
		gambarPause = GameObject.Find ("pausemenu").GetComponent<SpriteRenderer> ();
		bgGelap = GameObject.Find ("bgGelap").GetComponent<SpriteRenderer> ();
		btn_kd = GameObject.Find ("btn_kd").GetComponent<Button>();
		btn_materi = GameObject.Find ("btn_materi").GetComponent<Button>();
		btn_kumpulanrumus = GameObject.Find ("btn_kumpulanrumus").GetComponent<Button>();
		btn_simulasi = GameObject.Find ("btn_simulasi").GetComponent<Button>();
		btn_evaluasi= GameObject.Find ("btn_evaluasi").GetComponent<Button> ();

		btn_simulasi.onClick.AddListener (delegate() {
			Application.LoadLevel("menuSimulasi");
		});
		btn_kd.onClick.AddListener (delegate() {
			Application.LoadLevel("KD");
		});
		btn_kumpulanrumus.onClick.AddListener (delegate() {
			Application.LoadLevel("menuRumus");
		});
		btn_materi.onClick.AddListener (delegate() {
			Application.LoadLevel("menuMateri");
		});
		btn_evaluasi.onClick.AddListener (delegate() {
			Application.LoadLevel("soal");
		});

	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			canvasPause = GameObject.Find ("CanvasExitMenu").GetComponent<Canvas> ();
			canvasPause.enabled = true;
			gambarPause.enabled = true;
			bgGelap.enabled = true;
			canvasMenu = GameObject.Find ("CanvasMenu").GetComponent<Canvas> ();
			canvasMenu.enabled = false;
		}
	}
}
