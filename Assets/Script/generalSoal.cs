﻿using UnityEngine;
using System.Collections;

public class generalSoal : MonoBehaviour 
{
	private GameObject soal1;
	private GameObject soal2;
	private int lagiPindah = 0;
	private int nomorEvaluasi = 1;
	public int nomorPembahasan = 1;
	private string nomorPembahasanAsli = "";
	private bool udahPembahasan = false;
	public bool udahRapih = true;
	public int[] dataJawaban = new int[50]{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	public string[] jawabanBener = new string[50]{"b","e","c","c","a","d","b","b","a","e","d","d","c","c","a","e","d","c","c","a","d","b","c","c","c","d","a","d","b","e","c","d","c","b","d","c","a","a","d","d","a","b","b","e","a","c","c","e","a","e"};
	
	private string arahPindah = "kiri";

	private Ray raynya;
	private RaycastHit rayCasHit;

	void Update () 
	{
		if (udahRapih && Input.GetMouseButtonDown (0)) 
		{
			raynya = Camera.main.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast (raynya, out rayCasHit)) 
			{
				if (rayCasHit.transform.name == "tombolKePembahasan")
				{
					pindahKePembahasan();
				}
				else if (rayCasHit.transform.name == "tombolSebelumnya")
				{
					if(nomorPembahasan > 1)
					{
						int pembahasanBerikut = nomorPembahasan;
						pembahasanBerikut = pembahasanBerikut - 1;
						pindahPembahasan(GameObject.Find("pembahasan" + (nomorPembahasan).ToString()), GameObject.Find("pembahasan" + pembahasanBerikut.ToString()), "kanan");	
					}
				}
				else if (rayCasHit.transform.name == "tombolBerikutnya")
				{
					if(nomorPembahasan < 66)
					{
						int pembahasanBerikut = nomorPembahasan;
						pembahasanBerikut = pembahasanBerikut + 1;
						pindahPembahasan(GameObject.Find("pembahasan" + (nomorPembahasan).ToString()), GameObject.Find("pembahasan" + pembahasanBerikut.ToString()), "kiri");	
					}
				}
				else if (rayCasHit.transform.name == "tombolHome")
				{
					Application.LoadLevel("TampilanKetiga");
					Debug.Log("qweqwe");
				}
			}
		}
		if(!udahRapih)
		{
			if(lagiPindah > 0)
			{
				if(lagiPindah <= 350)
				{
					if(arahPindah == "kiri")
					{
						soal1.transform.position = new Vector3((float)(lagiPindah - 350) / 100f, 0, 0);
						soal2.transform.position = new Vector3((float)lagiPindah / 100f, 0, 0);
					}
					else
					{
						soal1.transform.position = new Vector3((float)((lagiPindah - 350) * -1) / 100f, 0, 0);
						soal2.transform.position = new Vector3((float)(lagiPindah * -1) / 100f, 0, 0);
					}
				}
				lagiPindah -= 5;
			}
			else if(lagiPindah == 0)
			{
				soal1.transform.position = new Vector3(-3.5f, 0, 0);
				soal2.transform.position = new Vector3(0f, 0, 0);
				if(!udahPembahasan)
				{
					GameObject.Find("evaluasi").GetComponent<MeshRenderer>().enabled = true;
					GameObject.Find("evaluasi").GetComponent<TextMesh>().text = "Evaluasi " + nomorEvaluasi.ToString() + " dari 50";
				}
				else
				{
					GameObject.Find ("evaluasi").GetComponent<MeshRenderer> ().enabled = true;
					GameObject.Find ("evaluasi").GetComponent<TextMesh> ().text = "Pembahasan";
					GameObject.Find ("evaluasiIjo").GetComponent<SpriteRenderer> ().enabled = true;
					GameObject.Find ("garisNomorPembahasan").GetComponent<SpriteRenderer> ().enabled = true;
					GameObject.Find ("nomorPembahasan").GetComponent<MeshRenderer> ().enabled = true;
					GameObject.Find ("nomorPembahasan").GetComponent<TextMesh> ().text = "Nomor " + nomorPembahasanAsli.ToString();
				}
				udahRapih = true;
			}
		}
	}
	public void pindahSoal(GameObject soal1q, GameObject soal2q, int nomorEvaluasiq)
	{
		udahRapih = false;
		soal1 = soal1q;
		soal2 = soal2q;
		nomorEvaluasi = nomorEvaluasiq;
		lagiPindah = 420;
		arahPindah = "kiri";
	}
	public void pindahCekNilai(GameObject soal1q, GameObject soal2q)
	{
		udahRapih = false;
		soal1 = soal1q;
		soal2 = soal2q;
		lagiPindah = 420;
		GameObject.Find ("evaluasi").GetComponent<MeshRenderer> ().enabled = false;
		GameObject.Find ("evaluasiIjo").GetComponent<SpriteRenderer> ().enabled = false;
		GameObject.Find ("evaluasi").GetComponent<MeshRenderer> ().enabled = true;
		int nilai = 0;
		for(int a = 1; a <= 50; a++)
		{
			if(dataJawaban[a - 1] == 0)
			{
				GameObject.Find("cekBenar" + a.ToString()).GetComponent<SpriteRenderer>().enabled = false;
			}
			else
			{
				GameObject.Find("cekSalah" + a.ToString()).GetComponent<SpriteRenderer>().enabled = false;
				nilai = nilai + 2;
			}
		}
		
		GameObject.Find ("nilainya").GetComponent<TextMesh> ().text = "Nilai Kamu : " + nilai.ToString ();
		GameObject.Find ("tombolKePembahasan").GetComponent<SpriteRenderer> ().enabled = true;
		GameObject.Find ("tombolKePembahasan").transform.position = new Vector3 (0.85f, -2.71f, -5f);
		arahPindah = "kiri";

	}
	public void pindahKePembahasan()
	{
		udahRapih = false;
		udahPembahasan = true;
		soal1 = GameObject.Find ("cekNilai");
		soal2 = GameObject.Find ("pembahasan1");
		lagiPindah = 420;
		GameObject.Find ("tombolKePembahasan").GetComponent<SpriteRenderer> ().enabled = false;
		GameObject.Find ("tombolKePembahasan").transform.position = new Vector3 (0.85f, -4.39f, -5f);
		GameObject.Find ("tombolBerikutnya").GetComponent<SpriteRenderer> ().enabled = true;
		GameObject.Find ("tombolBerikutnya").transform.position = new Vector3 (0.95f, -2.71f, -5f);
		GameObject.Find ("tombolSebelumnya").GetComponent<SpriteRenderer> ().enabled = true;
		GameObject.Find ("tombolSebelumnya").transform.position = new Vector3 (-0.87f, -2.71f, -5f);
		GameObject.Find ("tombolHome").GetComponent<SpriteRenderer> ().enabled = true;
		GameObject.Find ("tombolHome").transform.position = new Vector3 (-1.29f, 2.69f, -5f);
		arahPindah = "kiri";
		nomorPembahasanAsli = "1";
	}
	public void pindahPembahasan(GameObject soal1q, GameObject soal2q, string arahq)
	{
		udahRapih = false;
		soal1 = soal1q;
		soal2 = soal2q;
		lagiPindah = 420;
		if(arahq == "kiri")
		{
			nomorPembahasan++;
		}
		else
		{
			nomorPembahasan--;
		}
		arahPindah = arahq;
		nomorPembahasanAsli = soal2.GetComponent<pembahasan>().nomorAsli;
	}
}