﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class pauseHandle : MonoBehaviour {
	public Button _yes;
	public Button _no;
	public Canvas menuExit;
	public Canvas canvasMenu;
	public SpriteRenderer gambarPause;
	public SpriteRenderer bgGelap;
	public SpriteRenderer jendelaKiri;
	public SpriteRenderer jendelaKanan;
	// Use this for initialization
	void Start () {
		menuExit = GameObject.Find ("CanvasExitMenu").GetComponent<Canvas> ();
		canvasMenu = GameObject.Find ("CanvasMenu").GetComponent<Canvas> ();
		gambarPause = GameObject.Find ("pausemenu").GetComponent<SpriteRenderer> ();
		jendelaKiri = GameObject.Find ("jendelaKiri").GetComponent<SpriteRenderer> ();
		jendelaKanan = GameObject.Find ("jendelaKanan").GetComponent<SpriteRenderer> ();
		bgGelap = GameObject.Find ("bgGelap").GetComponent<SpriteRenderer> ();
		_yes = GameObject.Find ("btnYa").GetComponent<Button> ();
		_no = GameObject.Find ("btnTidak").GetComponent<Button> ();
		_yes.onClick.AddListener (delegate() {
			Application.Quit(); 
		});
		_no.onClick.AddListener (delegate() {
			canvasMenu.enabled = true;
			menuExit.enabled = false;
			bgGelap.enabled = false;
			gambarPause.enabled = false;
			jendelaKiri.enabled = false;
			jendelaKanan.enabled = false;
		});
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
