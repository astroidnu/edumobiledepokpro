﻿
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RotasiPerhitungan : MonoBehaviour {
	public Text txtm,txtn,txtalpha;//,txtx1,txty1;
	public Text alphaa1,alphab1,alphaa2,alphab2,ba,bb,x,y,aa,ab,ma,mb,na,nb;
	float a,m,alpha,b,n,hx,hy;
	float alphaC,alphaS;
	public Image testImage;
	bool simulrot = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		alphaa1.text = txtalpha.text;
		alphab1.text = txtalpha.text;
		alphaa2.text = txtalpha.text;
		alphab2.text = txtalpha.text;
		aa.text = "6".ToString();
		ab.text = "6".ToString();
		ba.text = "0".ToString();
		bb.text = "0".ToString();
		ma.text = txtm.text;
		mb.text = txtm.text;
		na.text = txtn.text;
		nb.text = txtn.text;

		a = 6f;
		b = 0f;
		m = float.Parse (txtm.text);
		n = float.Parse (txtn.text);
		alpha = float.Parse (txtalpha.text);
		if (alpha == 90) {
			alphaC = 0f;
			alphaS = 1f;
		}else if(alpha == 60){
			alphaC = 0.5f;
			alphaS = 0.86f;
		}
		else if(alpha == 45){
			alphaC = 0.7f;
			alphaS = 0.7f;
		}
		else if(alpha == 30){
			alphaC = 0.86f;
			alphaS = 0.5f;
		}
		else if(alpha == 0){
			alphaC = 1f;
			alphaS = 0f;
		}
		hx = ((a-m)*alphaC)-((b-n)*alphaS)+m;
		hy = ((a-m)*alphaS)+((b-n)*alphaC)+n;
		x.text = hx.ToString();
		y.text = hy.ToString();

		//SIMULASI
		if (simulrot == true) {
			simulrottrue();
		} else {
			simulrotfalse();
		}
	}

	void simulrottrue(){
		if(m == 0 && n ==0){
			if (alpha == 90) {
				Sprite newSprite =  Resources.Load <Sprite>("rot0090");
				if (newSprite){
					testImage.sprite = newSprite;
				} else {
					Debug.LogError("Sprite not found", this);
				}
			}else if(alpha == 60){
				Sprite newSprite =  Resources.Load <Sprite>("rot0060");
				if (newSprite){
					testImage.sprite = newSprite;
				} else {
					Debug.LogError("Sprite not found", this);
				}
			}
			else if(alpha == 45){
				Sprite newSprite =  Resources.Load <Sprite>("rot0045");
				if (newSprite){
					testImage.sprite = newSprite;
				} else {
					Debug.LogError("Sprite not found", this);
				}
			}
			else if(alpha == 30){
				Sprite newSprite =  Resources.Load <Sprite>("rot0030");
				if (newSprite){
					testImage.sprite = newSprite;
				} else {
					Debug.LogError("Sprite not found", this);
				}
			}
			else if(alpha == 0){
				Sprite newSprite =  Resources.Load <Sprite>("rot000");
				if (newSprite){
					testImage.sprite = newSprite;
				} else {
					Debug.LogError("Sprite not found", this);
				}
			}
		}
		else{
			if (alpha == 90) {
				Sprite newSprite =  Resources.Load <Sprite>("rot2290");
				if (newSprite){
					testImage.sprite = newSprite;
				} else {
					Debug.LogError("Sprite not found", this);
				}
			}else if(alpha == 60){
				Sprite newSprite =  Resources.Load <Sprite>("rot2260");
				if (newSprite){
					testImage.sprite = newSprite;
				} else {
					Debug.LogError("Sprite not found", this);
				}
			}
			else if(alpha == 45){
				Sprite newSprite =  Resources.Load <Sprite>("rot2245");
				if (newSprite){
					testImage.sprite = newSprite;
				} else {
					Debug.LogError("Sprite not found", this);
				}
			}
			else if(alpha == 30){
				Sprite newSprite =  Resources.Load <Sprite>("rot2230");
				if (newSprite){
					testImage.sprite = newSprite;
				} else {
					Debug.LogError("Sprite not found", this);
				}
			}
			else if(alpha == 0){
				Sprite newSprite =  Resources.Load <Sprite>("rot220");
				if (newSprite){
					testImage.sprite = newSprite;
				} else {
					Debug.LogError("Sprite not found", this);
				}
			}
		}

	}

	void simulrotfalse(){
		if(m == 0 && n ==0){
			Sprite newSprite =  Resources.Load <Sprite>("rot000");
			if (newSprite){
				testImage.sprite = newSprite;
			} else {
				Debug.LogError("Sprite not found", this);
			}
		}
		else{
			Sprite newSprite =  Resources.Load <Sprite>("rot220");
			if (newSprite){
				testImage.sprite = newSprite;
			} else {
				Debug.LogError("Sprite not found", this);
			}
		}
	}

	public void simulasi(){
		simulrot = true;
	}

	public void reset(){
		Sprite newSprite =  Resources.Load <Sprite>("rot000");
		if (newSprite){
			testImage.sprite = newSprite;
		} else {
			Debug.LogError("Sprite not found", this);
		}
		txtalpha.text = "0";
		txtm.text = "0";
		txtn.text = "0";
		simulrot = false;
	}
}
