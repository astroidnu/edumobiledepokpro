﻿using UnityEngine;
using System.Collections;

public class soal : MonoBehaviour 
{
	private string jawabanBener;
	private string nomorSoal;

	private Ray raynya;
	private RaycastHit rayCasHit;

	private generalSoal generalq;
	private bool udahKejawab = true;

	void Start () 
	{
		generalq = GameObject.Find ("general").GetComponent<generalSoal> ();
		nomorSoal = gameObject.name.Substring (4, gameObject.name.Length - 4);
		jawabanBener = generalq.jawabanBener [int.Parse(nomorSoal) - 1];
	}

	void Update () 
	{
		if (generalq.udahRapih && Input.GetMouseButtonDown (0)) 
		{
			raynya = Camera.main.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast (raynya, out rayCasHit)) 
			{
				if (rayCasHit.transform.name == "soal" + nomorSoal + "a" || rayCasHit.transform.name == "soal" + nomorSoal + "b" || rayCasHit.transform.name == "soal" + nomorSoal + "c" || rayCasHit.transform.name == "soal" + nomorSoal + "d" || rayCasHit.transform.name == "soal" + nomorSoal + "e") 
				{
					if(rayCasHit.transform.name == "soal" + nomorSoal + jawabanBener)
					{
						generalq.dataJawaban[int.Parse(nomorSoal) - 1] = 1;
					}
					else
					{
						generalq.dataJawaban[int.Parse(nomorSoal) - 1] = 0;
					}
					rayCasHit.transform.gameObject.GetComponent<SpriteRenderer>().enabled = true;
					int soalBerikut = int.Parse(nomorSoal);
					soalBerikut = soalBerikut + 1;
					if(soalBerikut <= 50)
					{
						generalq.pindahSoal(GameObject.Find("soal" + (int.Parse(nomorSoal)).ToString()), GameObject.Find("soal" + soalBerikut.ToString()), soalBerikut);
					}
					else
					{
						generalq.pindahCekNilai(GameObject.Find("soal" + (int.Parse(nomorSoal)).ToString()), GameObject.Find("cekNilai"));
					}
				}
			}			
		}
	}
}
